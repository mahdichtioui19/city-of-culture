package citeculture.hamzajeljeli.io.citdelaculture.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import citeculture.hamzajeljeli.io.citdelaculture.Beans.InfoBean;
import citeculture.hamzajeljeli.io.citdelaculture.R;

public class gridAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<InfoBean> pics;

    // 1
    public gridAdapter(Context context, List<InfoBean> pics) {
        this.mContext = context;
        this.pics = pics;
    }

    // 2
    @Override
    public int getCount() {
        return pics.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }

    // 5
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final InfoBean infoBean = pics.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.layout_grid, null);
        }
        final ImageView img = (ImageView) convertView.findViewById(R.id.iv_img);

        final View finalConvertView = convertView;
        Glide.with(mContext).load(infoBean.getBanner()).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                img.setBackground(resource);
            }
        });
        return convertView;
    }

}
